'use strict';
const ADODB = require('node-adodb');
const connection = ADODB.open('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb;');

 async function buildSystemTables(){
    return await buildtable('TESTTABLE_S','id  INTEGER  PRIMARY KEY,name CHAR (15)');
  }
  
  async function buildtable(tablename, tableVariables) {
    try {
  
      const entireCommand = `CREATE TABLE ${tablename} (${tableVariables})`
  
      return await connection
        .execute(`${entireCommand}`)
        .then( async (data) => {
          return data;
        })
        .catch(error => {
          return error;
        });
    } catch (error) {
      console.error(error);
    }
  }


module.exports = { 
    
    buildSystemTables,
    buildtable
}    