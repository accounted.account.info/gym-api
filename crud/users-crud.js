'use strict';
const ADODB = require('node-adodb');
const connection = ADODB.open('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb;');

async function saveNewUser( userObject ) {
    try {
      const objectToSave = {
        lastname: userObject.lastname,
        name: userObject.name,
        DEFAULTDEPTID: (userObject.planId) ? userObject.planId : null,
        Gender: (userObject.gender) ? userObject.gender : '',
        ATT: 1,
        OUTEARLY: 1,
        OVERTIME: 1,
        SEP: 1,
        HOLIDAY: 1,
        LUNCHDURATION: 1,
        InheritDeptSch: 1,
        InheritDeptSchClass: 1,
        AutoSchPlan: 1,
        MinAutoSchInterval: 24,
        RegisterOT: 1,
        InheritDeptRule: 1,
        OPHONE: (userObject.phone) ? userObject.phone : '',        
        email: (userObject.email) ? userObject.email : '',
        BIRTHDAY: `#${userObject.planStartDate}#`,
        HIREDDAY: `#${userObject.planEndDate}#`,
        OfflineEndDate: `#${userObject.planEndDate}#`,
        LUNCHDURATION: (userObject.sesiones) ? userObject.sesiones : 3
      }

      let command = '';
      let valuesCommand = '';
      let started = false;

      for (const property in objectToSave) {
          if (started) {
              command = `${command},${property}`
              if (objectToSave[property] ===  null || property === 'BIRTHDAY' || property === 'HIREDDAY' || property === 'OfflineEndDate') {
                  valuesCommand = `${valuesCommand},${objectToSave[property]}`
              } else {
                  valuesCommand = `${valuesCommand},\"${objectToSave[property]}\"`;
              }              
          } else {
              started = true;
              command = `${property}`;        
              if (objectToSave[property] ===  null || property === 'BIRTHDAY' || property === 'HIREDDAY' || property === 'OfflineEndDate') {
                valuesCommand = `${objectToSave[property]}`
            } else {
                valuesCommand = `\"${objectToSave[property]}\"`;
            }   
          }
      }
      const table = 'USERINFO';
      const entireCommand = `INSERT INTO ${table}(${command}) VALUES (${valuesCommand})`
      console.log(entireCommand);

      return await connection
        .execute(`${entireCommand}`)
        .then( async (data) => {
          await updateFixUser();
          return await queryUsers();
        })
        .catch(error => {
          console.error(error);
        });
    } catch (error) {
      console.error(error);
    }
}

async function updateFixUser( userObject ) {
  try {
    const table = 'USERINFO';
    const entireCommand = `UPDATE ${table} SET Badgenumber = USERID WHERE 1 `
    //console.log(entireCommand);

    return await connection
      .execute(`${entireCommand}`)
      .then( async (data) => {
        return await queryUsers();
      })
      .catch(error => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}

async function updateUser( userObject ) {
  try {

    const idToUpdate = userObject.id;

    const objectToUpdate = {
      Badgenumber: `${userObject.id}`,
      lastname: userObject.lastname,
      name: userObject.name,
      DEFAULTDEPTID: userObject.planId,
      BIRTHDAY: `#${userObject.planStartDate}#`,
      OfflineEndDate: `#${userObject.planEndDate}#`,
      LUNCHDURATION: (userObject.sesiones) ? userObject.sesiones : 3,
      OPHONE: (userObject.phone) ? userObject.phone : '',        
      email: (userObject.email) ? userObject.email : '',
    }

    let command = '';
    let started = false;

    for (const property in objectToUpdate) {
      if (objectToUpdate[property] ===  null || property === 'BIRTHDAY' || property === 'OfflineEndDate') {
        command = `${(started) ? `${command},`:''} ${property} = ${objectToUpdate[property]}`
      } else {
        command = `${(started) ? `${command},`:''} ${property} = \"${objectToUpdate[property]}\"`
      }
      started = true;    
    }

    const table = 'USERINFO';
    const entireCommand = `UPDATE ${table} SET ${command} WHERE [USERID]=${idToUpdate}`
    //console.log(entireCommand);

    return await connection
      .execute(`${entireCommand}`)
      .then( async (data) => {
        return await queryUsers();
      })
      .catch(error => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}


async function deleteUser( userObject ) {
  try {

    const idToDelete = userObject.id;
    let command = '';
    let started = false;

    const entireCommand = `DELETE FROM USERINFO WHERE [USERID]=${idToDelete}`;
    const entireCommand2 = `DELETE FROM BioTemplate WHERE [USERID]=${idToDelete}`;
    const entireCommand3 = `DELETE FROM CHECKINOUT WHERE [USERID]=${idToDelete}`;

    connection
      .execute(`${entireCommand2}`);
    connection
      .execute(`${entireCommand3}`);
    return await connection
      .execute(`${entireCommand}`)
      .then( async (data) => {
        return await queryUsers();
      })
      .catch(error => {
        console.error(error);
      });
  } catch (error) {
    console.error(error);
  }
}

async function queryUsers() {
  try {
    const users = await connection.query('SELECT USERID as id, DEPARTMENTS.DEPTNAME as planName, DEPARTMENTS.DEPTID as planId , name, lastname, Gender as gender, Format([BIRTHDAY],"yyyy-mm-dd hh:mm:ss") as planStartDate, Format([OfflineEndDate],"yyyy-mm-dd hh:mm:ss")  as planEndDate, USERINFO.LUNCHDURATION as sesiones,  OPHONE as phone , email FROM USERINFO INNER JOIN DEPARTMENTS ON DEPARTMENTS.DEPTID = USERINFO.DEFAULTDEPTID ORDER BY [OfflineEndDate] ASC');
    return users;
  } catch (error) {
    return error;
  }
}

module.exports = { 
    saveNewUser, 
    queryUsers, 
    updateUser, 
    deleteUser
}