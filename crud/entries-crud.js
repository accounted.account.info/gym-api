'use strict';
const ADODB = require('node-adodb');
const connection = ADODB.open('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb;');

function dateToString(aDate) {
    try {
        return `${aDate.getFullYear()}-${(String(aDate.getMonth()+1).padStart(2, '0'))}-${String(aDate.getDate()).padStart(2, '0')}`;
    } catch (error) {
        return '';
    }
  }

  function fulldateString( aDate )  {
    try {
        const fullDate = `${aDate.getFullYear()}-${(String(aDate.getMonth()+1).padStart(2, '0'))}-${String(aDate.getDate()).padStart(2, '0')}`;
        const fulltime = `${(String(aDate.getHours()).padStart(2, '0'))}:${(String(aDate.getMinutes()).padStart(2, '0'))}:${(String(aDate.getSeconds()).padStart(2, '0'))}`;
        return `${fullDate} ${fulltime}`;
    } catch (error) {
        
    }
  }
    
  function getNowTimeDate() {
    const nowDate = new Date();
    return fulldateString( nowDate );
  }
  
  async function queryEntries() {
    try {
      const curr = new Date; // get current date
      const curr2 = new Date;
      const first = curr.getDate() - curr.getDay() + ( curr.getDay() == 0 ? -6:1) ; 
      const last_ = curr.getDate() + ( curr.getDay() == 0 ? 1:7) ; 
      const last = last_ - curr.getDay();    
      const lastday = dateToString(new Date(curr2.setDate(last)));
      const firstday = dateToString(new Date(curr.setDate(first)));
  
      const entries = await connection.query(`SELECT LOGID as id, 0 as counting,CHECKINOUT.USERID as userid, Format([CHECKINOUT.CHECKTIME],"yyyy-mm-dd hh:mm:ss") as checktime, USERINFO.name as name, USERINFO.lastname as lastname, DEPARTMENTS.DEPTNAME as planName,  Format([USERINFO.OfflineEndDate],"yyyy-mm-dd hh:mm:ss") as planEndDate, USERINFO.LUNCHDURATION as sesiones, CHECKTYPE as tipo  FROM ( CHECKINOUT INNER JOIN USERINFO ON USERINFO.USERID = CHECKINOUT.USERID ) INNER JOIN DEPARTMENTS ON DEPARTMENTS.DEPTID = USERINFO.DEFAULTDEPTID WHERE CHECKTIME BETWEEN #${firstday}# AND #${lastday}# ORDER BY CHECKTIME DESC`);
      const counterQuery = `SELECT CHECKINOUT.USERID as id, COUNT(*) as counting FROM CHECKINOUT INNER JOIN USERINFO ON USERINFO.USERID =  CHECKINOUT.USERID WHERE CHECKTIME BETWEEN #${firstday}# AND #${lastday}# GROUP BY CHECKINOUT.USERID`;
      const entriesCounter = await connection.query(counterQuery);
      entries.map( ( element ) => {
        const found = entriesCounter.find( item => item.id === element.userid );
        element.counting = found.counting; 
        return element;    
      });   
      return entries;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
  
  
  async function queryOldEntries() {
    try {
      const curr = new Date; // get current date
      const curr2 = new Date;
      const first = curr.getDate() - curr.getDay() + ( curr.getDay() == 0 ? -6:1) ; 
      const last_ = curr.getDate() + ( curr.getDay() == 0 ? 1:7) ; 
      const last = last_ - curr.getDay();    
      const firstday = dateToString(new Date(curr.setDate(first)));
  
      const entries = await connection.query(`SELECT LOGID as id,CHECKINOUT.USERID as userid, Format([CHECKINOUT.CHECKTIME],"yyyy-mm-dd hh:mm:ss") as checktime, USERINFO.name as name, USERINFO.lastname as lastname, DEPARTMENTS.DEPTNAME as planName,  Format([USERINFO.OfflineEndDate],"yyyy-mm-dd hh:mm:ss") as planEndDate, USERINFO.LUNCHDURATION as sesiones, CHECKTYPE as tipo  FROM ( CHECKINOUT INNER JOIN USERINFO ON USERINFO.USERID = CHECKINOUT.USERID ) INNER JOIN DEPARTMENTS ON DEPARTMENTS.DEPTID = USERINFO.DEFAULTDEPTID WHERE CHECKTIME < #${firstday}# ORDER BY CHECKTIME DESC`); 
      return entries;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
  
  async function saveNewEntry( entryObj ) {
    
    try {
      const objectToSave = {
        USERID: entryObj.userId,
        CHECKTIME: `#${getNowTimeDate()}#`,
        VERIFYCODE: 1,
        WorkCode: 0,
        sn: 'AIOR212360658',
        UserExtFmt: 0,
        CHECKTYPE: 'M'
      }
  
      if ( entryObj.USERID === null || entryObj.CHECKTIME === null ) return null
  
      let command = '';
      let valuesCommand = '';
      let started = false;
  
      for (const property in objectToSave) {
          if (started) {
              command = `${command},${property}`
              if (objectToSave[property] ===  null || property === 'CHECKTIME' || property === 'VERIFYCODE' || property === 'WorkCode' || property === 'UserExtFmt' ) {
                  valuesCommand = `${valuesCommand},${objectToSave[property]}`
              } else {
                  valuesCommand = `${valuesCommand},\"${objectToSave[property]}\"`;
              }              
          } else {
              started = true;
              command = `${property}`;        
              valuesCommand = `${objectToSave[property]}`;
          }
      }
      const table = 'CHECKINOUT';
      const entireCommand = `INSERT INTO ${table}(${command}) VALUES (${valuesCommand})`
      console.log(entireCommand);
  
      return await connection
        .execute(`${entireCommand}`)
        .then( async (data) => {
          return await queryEntries();
        })
        .catch(error => {
          console.error(error);
        });
    } catch (error) {
      console.error(error);
    }
  }

  
  async function deleteEntry( entryId ) {
    
    try {
      const table = 'CHECKINOUT'
      const entireCommand = `DELETE FROM ${table} WHERE LOGID=${entryId}`
      console.log(entireCommand);
  
      return await connection
        .execute(`${entireCommand}`)
        .then( async (data) => {
          return await queryEntries();
        })
        .catch(error => {
          console.error(error);
        });
    } catch (error) {
      console.error(error);
    }
  }

  
module.exports = { 
    queryEntries,
    saveNewEntry,
    queryOldEntries,
    deleteEntry
}