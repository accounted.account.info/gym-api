'use strict';
const ADODB = require('node-adodb');
const connection = ADODB.open('Provider=Microsoft.Jet.OLEDB.4.0;Data Source=db.mdb;');

async function saveNewDepartment( depObject ) {
    try {
      const objectToSave = {
        DEPTID: depObject.id,
        DEPTNAME: `${depObject.name}`,
        code: depObject.id,
        SUPDEPTID: 0
      }
  
      if ( depObject.DEPTID === null || depObject.DEPTNAME === null ) return null
  
      let command = '';
      let valuesCommand = '';
      let started = false;
  
      for (const property in objectToSave) {
          if (started) {
              command = `${command},${property}`
              if (objectToSave[property] ===  null) {
                  valuesCommand = `${valuesCommand},${objectToSave[property]}`
              } else {
                  valuesCommand = `${valuesCommand},\"${objectToSave[property]}\"`;
              }              
          } else {
              started = true;
              command = `${property}`;        
              valuesCommand = `${objectToSave[property]}`;
          }
      }
      const table = 'DEPARTMENTS';
      const entireCommand = `INSERT INTO ${table}(${command}) VALUES (${valuesCommand})`
      console.log(entireCommand);
  
      return await connection
        .execute(`${entireCommand}`)
        .then( async (data) => {
          return await queryDeparments();
        })
        .catch(error => {
          console.error(error);
        });
    } catch (error) {
      console.error(error);
    }
  }
  
  
  async function updateDepartment( depObject ) {
    try {
  
      const idToUpdate = depObject.id;
  
      const objectToUpdate = {
        DEPTNAME: `${depObject.name}`,
        code: depObject.id,
        SUPDEPTID: 0
      }
  
      let command = '';
      let started = false;
  
      for (const property in objectToUpdate) {
        if (objectToUpdate[property] ===  null) {
          command = `${(started) ? `${command},`:''} ${property} = ${objectToUpdate[property]}`
        } else {
          command = `${(started) ? `${command},`:''} ${property} = \"${objectToUpdate[property]}\"`
        }
        started = true;    
      }
  
      const table = 'DEPARTMENTS';
      const entireCommand = `UPDATE ${table} SET ${command} WHERE [DEPTID]=${idToUpdate}`
      console.log(entireCommand);
  
      return await connection
        .execute(`${entireCommand}`)
        .then( async (data) => {
          return await queryDeparments();
        })
        .catch(error => {
          console.error(error);
        });
    } catch (error) {
      console.error(error);
    }
  }
  
  async function queryDeparments() {
    try {
      const users = await connection.query('SELECT DEPTID as id, DEPTNAME as name FROM DEPARTMENTS');
      return users;
    } catch (error) {
      return error;
    }
  }

module.exports = { 
    saveNewDepartment, 
    updateDepartment, 
    queryDeparments
  }