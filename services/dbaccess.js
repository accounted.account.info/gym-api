'use strict';


var usersCrud = require('../crud/users-crud');
var departmentsCrud = require('../crud/departments-crud');
var entriestCrud = require('../crud/entries-crud');
var systemCrud = require('../crud/system-crud');

////////////////////////////////


module.exports = { 
  usersCrud,
  departmentsCrud, 
  entriestCrud,
  systemCrud
}
