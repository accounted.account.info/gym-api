'use strict'; 
var dbaccess = require('../services/dbaccess.js');

var controllers = { 
    about: async function(req,res){ 
        const entries = await dbaccess.usersCrud.queryUsers(); 
        res.json(entries);
    }, 
    getUsers: async function(req, res){ 
        const users = await dbaccess.usersCrud.queryUsers(); 
        res.json(users);
    }, 
    newUser: async function(req, res){ 
        console.dir(req.body);
        const users = await dbaccess.usersCrud.saveNewUser(req.body); 
        res.json(users);
    }, 
    updateUser: async function(req, res){ 
        console.dir(req.body);
        const users = await dbaccess.usersCrud.updateUser(req.body); 
        res.json(users);
    }, 
    deleteUser: async function(req, res){ 
        console.dir(req.body);
        const users = await dbaccess.usersCrud.deleteUser(req.body); 
        res.json(users);
    },  
    deleteUserById: async function(req, res){ 
        const user = { id : req.params.id }
        const users = await dbaccess.usersCrud.deleteUser(user); 
        res.json(users);
    },  
    getDepartments: async function(req, res){ 
        const users = await dbaccess.departmentsCrud.queryDeparments(); 
        res.json(users);
    }, 
    newDepartment: async function(req, res){ 
        console.dir(req.body);
        const users = await dbaccess.departmentsCrud.saveNewDepartment(req.body); 
        res.json(users);
    }, 
    updateDepartment: async function(req, res){ 
        console.dir(req.body);
        const users = await dbaccess.departmentsCrud.updateDepartment(req.body); 
        res.json(users);
    }, 
    getEntries: async function(req, res){ 
        const entries = await dbaccess.entriestCrud.queryEntries(); 
        res.json(entries);
    },
    getOldEntries: async function(req, res){ 
        const entries = await dbaccess.entriestCrud.queryOldEntries(); 
        res.json(entries);
    },
    saveEntry: async function(req, res){ 
        const entries = await dbaccess.entriestCrud.saveNewEntry(req.body); 
        res.json(entries);
    },     
    deleteEntry: async function(req, res){ 
        const entries = await dbaccess.entriestCrud.deleteEntry(req.params.id); 
        res.json(entries);
    },
    test: async function(req, res){ 
        const entries = await dbaccess.entriestCrud.queryEntries(); 
        res.json(entries);
    }     
}; 

module.exports = controllers;