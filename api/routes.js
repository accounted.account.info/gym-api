'use strict';
const controller = require('./controller');

module.exports = (app) => {
    app.route('/users').get(controller.getUsers);

    app.route('/api/users').get(controller.getUsers);
    app.route('/api/users').post(controller.newUser);
    app.route('/api/users').put(controller.updateUser);    
    app.route('/api/users').delete(controller.deleteUser);    
    app.route('/api/users/:id').delete(controller.deleteUserById);

    app.route('/api/departments').get(controller.getDepartments);
    app.route('/api/departments').post(controller.newDepartment);
    app.route('/api/departments').put(controller.updateDepartment);

    app.route('/api/entries').get(controller.getEntries);
    app.route('/api/entries').post(controller.saveEntry); 
    app.route('/api/entries/:id').delete(controller.deleteEntry); 
    app.route('/api/old-entries').get(controller.getOldEntries);

    app.route('/api/test').get(controller.test);
}